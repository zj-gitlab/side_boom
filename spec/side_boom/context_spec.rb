RSpec.describe SideBoom::Context do
  subject { described_class }

  after do
    subject.clear
  end

  describe '.push' do
    let(:input) { 'string' }

    it 'adds values to the stack' do
      subject.push(input)

      expect(subject.peek).to be(input)
    end
  end

  describe '.pop' do
    let(:input) { (1..10).to_a }

    before do
      input.each { |i| subject.push(i) }
    end

    it 'removes the top value' do
      expect(subject.pop).to be(input.last)
    end
  end

  describe '.peek' do
    it 'shows the last pushed value' do
      subject.push(1)

      expect(subject.peek).to be(1)
    end

    it 'does not pop the value' do
      subject.push(1)

      expect(subject.peek).to be(1)
      expect(subject.peek).to be(1)
    end
  end
end

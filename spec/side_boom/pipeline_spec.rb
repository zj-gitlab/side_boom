require 'tmpdir'

RSpec.describe SideBoom::Pipeline do
  describe 'setting of attributes' do
    let(:image) { 'golang:latest' }

    subject { described_class.new(StringIO.new) }

    it 'allows setting of the image attribute' do
      subject.image(image)

      expect(subject.attributes).to have_key('image')
      expect(subject.attributes['image']).to eq(image)
    end
  end

  describe '#write' do
    let(:tmp_dir) { Dir.mktmpdir }
    let!(:sink) { File.join(tmp_dir, '.gitlab-ci.yml') }

    after do
      FileUtils.remove_entry(tmp_dir)
    end

    it 'writes the pipeline to the file' do
      pipeline =
        described_class.define(sink) do
          stage :test do
            job :rspec do
              script ['rspec']
            end
          end
        end

      expect(File.exist?(sink)).to be(true)
    end
  end
end

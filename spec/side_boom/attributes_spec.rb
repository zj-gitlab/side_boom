RSpec.describe SideBoom::Attributes do
  describe 'inherriting attributes' do
    context "when including stage's attributes" do
      let(:stage) do
        stage = SideBoom::Stage.new('test')
        stage.image 'ruby:2.4'
        stage
      end
      let(:pipeline) { SideBoom::Pipeline.new(StringIO.new) }

      let(:job) { SideBoom::Job.new('job') }

      it "retreives the stage's attributes" do
        allow(job).to receive(:stage).and_return(stage)
        allow(stage).to receive(:pipeline).and_return(pipeline)

        expect(job.attributes['image']).to be_nil
        expect(job.to_hash['image']).to eq('ruby:2.4')
      end
    end
  end
end

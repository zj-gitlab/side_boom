RSpec.describe SideBoom::Stage do
  subject { described_class.new('compile') }

  describe '#name' do
    it 'returns the name' do
      expect(subject.name).to eq('compile')
    end
  end

  describe 'defining jobs' do
    it 'adds the job to the jobs list' do
      job = subject.job('compile clang') { image('c') }

      expect(subject.jobs).to eq(job)
    end
  end
end

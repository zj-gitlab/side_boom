require 'byebug'

RSpec.describe SideBoom::Job do
  subject { described_class.new(':rspec 1') }

  let(:stage) { double(:stage, name: 'stage1') }

  before do
    SideBoom::Context.push(stage)
  end

  describe '#name' do
    it 'responds to #name' do
      expect(subject.name).to eq(':rspec 1')
    end
  end

  describe '#stage' do
    it 'is set on initialization' do
      expect(subject.stage.name).to eq(stage.name)
    end
  end
end

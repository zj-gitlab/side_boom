require 'open3'

RSpec.describe SideBoom do
  it 'has a version number' do
    expect(described_class::VERSION).to be_a(String)
  end

  describe 'integration tests' do
    let(:examples_dir) { File.expand_path('../examples', __dir__) }
    let(:output_files) { Dir[File.join(examples_dir, '*.out')] }

    it 'generates correct output' do
      output_files.each do |out|
        input = out.chomp('.out')
        output = bin_exec("#{input}.sideboom")

        expect(output).to eq(File.read(out))
      end
    end
  end

  def bin_exec(input_file)
    executable = File.expand_path('../bin/sideboom', __dir__)

    `#{executable} -f #{input_file}`
  end
end

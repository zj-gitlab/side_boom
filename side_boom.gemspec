lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'side_boom'

description = <<-DESC
  The .gitlab-ci.yml is hard to maintain if your project grows, and its hard to
  determine what is defined where, and why. This Ruby DSL allows you to not be
  limited by YAMLs limitations, but have all Ruby's features at your disposal.
DESC

Gem::Specification.new do |spec|
  spec.name          = 'side_boom'
  spec.version       = SideBoom::VERSION
  spec.authors       = ['Zeger-Jan van de Weg']
  spec.email         = ['git@zjvandeweg.nl']

  spec.summary       = 'Construct and maintain your GitLab CI/CD pipeline'
  spec.description   = description
  spec.homepage      = 'https://gitlab.com/zj/side_boom'
  spec.license       = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = ['sideboom']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'byebug', '~> 10.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end

module SideBoom
  # To keep the context during defining the pipeline, allowing for RSpec like
  # definitions, without explicitly linking a job to a stage. Example:
  #
  # SideBoom::Pipeline.define '.gitlab-ci.yml' do
  #   stage 'compile' do
  #     job 'golang 1.9' do
  #       # specifications here
  #     end
  #   end
  # end
  class Context
    @stack = []

    class << self
      def push(context)
        @stack.push(context)
      end

      def pop
        @stack.pop
      end

      def peek
        @stack.last
      end

      def clear
        @stack = []
      end

      def in(context)
        push(context)
        val = yield peek
        pop

        val
      end
    end
  end
end

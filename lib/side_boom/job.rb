# frozen_string_literal: true

module SideBoom
  class Job
    include SideBoom::Attributes

    attr_reader :name, :stage

    def initialize(name)
      @name = name
      @stage = SideBoom::Context.peek
    end

    def to_hash
      attrs = stage.merged_attributes.merge(attributes)
      attrs['stage'] = stage.name

      duplicated_values(attrs)
    end

    private

    # Avoid YAML to use inherritence, so output remains readable
    def duplicated_values(dup_hash)
      dup_hash.each_with_object({}) do |(key, value), hash|
        hash[key] = begin
                      value.dup
                    rescue TypeError # The unduppables
                      value
                    end
      end
    end
  end
end

module SideBoom
  ## Attributes define the methods that can be set on both Pipeline, Stage, and
  # Job level. Attributes set at pipeline level can be overriden at either stage-
  # or job-level. Stage level attributes can be overriden by any of its jobs.
  module Attributes
    class << self
      def boolean_attribute(*attrs)
        attrs.each do |attr|
          define_method("#{attr}!") do
            @attributes[attr.to_s] = true
          end

          define_method("dis#{attr}!") do
            @attributes[attr.to_s] = false
          end

          define_method(attr) do |arg|
            @attributes[attr.to_s] = !!arg
          end
        end
      end

      def string_attribute(*attrs)
        attrs.each do |attr|
          define_method(attr) do |arg|
            attributes[attr.to_s] = arg.to_s
          end
        end
      end

      def array_attribute(*attrs)
        attrs.each do |attr|
          define_method(attr) do |args|
            args = [args] if args.is_a?(String)

            attributes[attr.to_s] = args.map(&:to_s)
          end
        end
      end
    end

    boolean_attribute :allow_failure
    string_attribute :coverage, :image
    array_attribute :script, :before_script, :after_script

    def attributes
      @attributes ||= {}
    end
  end
end

# frozen_string_literal: true

module SideBoom
  class Stage
    include SideBoom::Attributes

    attr_reader :name, :jobs, :pipeline

    def initialize(name)
      @name = name
      @pipeline = SideBoom::Context.peek
      @jobs = []
    end

    def job(name, &block)
      SideBoom::Context.in(self) do
        new_job = SideBoom::Job.new(name)
        new_job.instance_eval(&block) if block_given?

        @jobs << new_job
      end
    end

    def merged_attributes
      pipeline.attributes.merge(attributes)
    end

    def to_hash
      @jobs.each_with_object({}) do |job, hash|
        hash[job.name] = job.to_hash
      end
    end
  end
end

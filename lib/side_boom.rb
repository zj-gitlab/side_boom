# frozen_string_literal: true
require 'yaml'

Dir[File.join(__dir__, 'side_boom', '*.rb')].sort.each { |file| require file }

module SideBoom
  VERSION = '0.1.0'.freeze
end
